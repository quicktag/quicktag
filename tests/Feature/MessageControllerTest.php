<?php

namespace Tests\Feature;

use App\Models\Message;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MessageControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_returns_a_list_of_messages_with_users()
    {
        $user = User::factory()->create();
        $messages = Message::factory()->count(3)->create(['user_id' => $user->id]);

        $response = $this->actingAs($user)->get('/api/messages');
//        $response->dd();
        $response->assertStatus(200);
        $response->assertJsonCount(3, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'created_at',
                        'updated_at',
                    ],
                    'message',
                    'created_at',
                    'updated_at',
                ]
            ],
            'links',
        ]);
    }

    /** @test */
    public function it_stores_a_new_message()
    {
        $user = User::factory()->create();
        $data = ['message' => 'Test message'];

        $response = $this->actingAs($user)->postJson('/api/messages', $data);

        $response->assertStatus(200);
        $this->assertDatabaseHas('messages', [
            'user_id' => $user->id,
            'message' => 'Test message',
        ]);
    }
}
