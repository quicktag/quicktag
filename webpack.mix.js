const mix = require('laravel-mix');

mix
    .setPublicPath('public')
    .css('resources/css/app.css', 'css')
    .js('resources/js/app.js', 'js')
    .vue()
    .sourceMaps()
    .version();
