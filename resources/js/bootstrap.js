import EchoLib from 'laravel-echo';

window.io = require('socket.io-client');

window.Echo = new EchoLib({
    broadcaster: 'socket.io',
    host: 'quicktag.localhost:6001',
});


