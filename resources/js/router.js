import {createRouter, createWebHistory} from 'vue-router'
import Chat from "./Components/Chat.vue";
import Auth from "./Components/Auth.vue";


const routes = [
    {
        path: '/',
        component: Chat,
    },
    {
        path: '/auth',
        component: Auth,
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
