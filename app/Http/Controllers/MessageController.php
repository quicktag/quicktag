<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageStoreRequest;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::with('user')->orderBy('created_at', 'desc')->paginate(10);
        return response()->json($messages);
    }

    public function store(MessageStoreRequest $request)
    {
        $user = Auth::user();

        if (! $user) {
            abort(401, 'Unauthenticated');
        }

        $message = $request->validated('message');

        $savedMessage = Message::create([
            'user_id' => $user->id,
            'message' => $message,
        ]);

        broadcast(new \App\Events\MessageSent($user->name, $savedMessage->message));

        return response()->json(['message' => 'Message sent']);
    }
}
