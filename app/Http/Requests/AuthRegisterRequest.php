<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Поле имя является обязательным',
            'name.string' => 'Поле имя должно быть строкой',
            'name.max' => 'Поле имя должно содержать не более :max символов',
            'email.required' => 'Поле электронной почты является обязательным',
            'email.string' => 'Поле электронной почты должно быть строкой',
            'email.email' => 'Неверный формат адреса электронной почты',
            'email.max' => 'Поле электронной почты должно содержать не более :max символов',
            'email.unique' => 'Указанный адрес электронной почты уже зарегистрирован',
            'password.required' => 'Поле пароля является обязательным',
            'password.string' => 'Поле пароля должно быть строкой',
            'password.confirmed' => 'Пароли не совпадают',
            'password.min' => 'Поле пароля должно содержать не менее :min символов'
        ];
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed', 'min:4'],
        ];
    }
}
