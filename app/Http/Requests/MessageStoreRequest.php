<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'message.required' => 'Пожалуйста, введите сообщение',
            'message.min' => 'Сообщение должно содержать не менее :min символов',
        ];
    }

    public function rules()
    {
        return [
            'message' => ['required', 'min:1'],
        ];
    }
}
