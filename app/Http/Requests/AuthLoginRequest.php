<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthLoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'email.required' => 'Поле электронной почты является обязательным',
            'email.string' => 'Поле электронной почты должно быть строкой',
            'email.email' => 'Неверный формат адреса электронной почты',
            'email.max' => 'Поле электронной почты должно содержать не более :max символов',
            'password.required' => 'Поле пароля является обязательным',
            'password.string' => 'Поле пароля должно быть строкой',
            'password.min' => 'Поле пароля должно содержать не менее :min символов'
        ];
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:4'],
        ];
    }
}
